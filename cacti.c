
#include <pthread.h>
#include <stddef.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include "cacti.h"
#include <signal.h>

_Thread_local actor_id_t idGlob;
int success = 0; 

actor_id_t actor_id_self() {
    return idGlob;
}

typedef struct queue {
    void **array;
    int i;
    size_t beg;
    size_t n;
    size_t counter;

} queue_t;

void *dequeue(queue_t *q);

void queue_new(queue_t **q) {
    //assert(q);
    *q = (queue_t *) malloc(sizeof(queue_t));
    (*q)->i = -1;
    (*q)->beg = 0;
    (*q)->n = 0;
    (*q)->counter = 0;
    (*q)->array = NULL;
}

void queue_destroy(queue_t *q) {

    int j;

    for (j = 0; j < q->counter; ++j) {
        void *temp = dequeue(q);
        free(temp);
    }
    
    free(q->array);
    free(q);
}

typedef struct tpool {
    pthread_t *array;
    size_t size;
    pthread_mutex_t *mutexQ;
    pthread_cond_t *condQ;

    queue_t *q;

} tpool_t;

static void *execute(void *pool);


int thread_pool_create(tpool_t *pool, size_t size) {

    pool->mutexQ = malloc(sizeof(pthread_mutex_t));
    pool->condQ = malloc(sizeof(pthread_cond_t));

    if (pthread_mutex_init(pool->mutexQ, NULL))
        perror("pool mutex init failed\n");
    if (pthread_cond_init(pool->condQ, NULL))
        perror("pool cond init failed\n");
    
    queue_new(&pool->q);

    pool->array = (pthread_t *) malloc(size * sizeof(pthread_t));
    pool->size = size;
    size_t i;
    for (i = 0; i < size; ++i) {
        if (pthread_create(&(pool->array[i]), NULL, execute, (void *) pool))
            perror("Failed to create the thread");
    }
}

void thread_pool_destroy(tpool_t *pool) {
    size_t i;
    for (i = 0; i < pool->size; ++i) {
        if (pthread_join(pool->array[i], NULL))
            perror("Failed to join the thread");
    }

    if (pthread_mutex_destroy(pool->mutexQ))
        perror("pool mutex destroy failed\n");

    if (pthread_cond_destroy(pool->condQ))
        perror("pool cond destroy failed\n");
    free(pool->mutexQ);
    free(pool->condQ);

    queue_destroy(pool->q);
  
    free(pool->array);

}

typedef struct actor {
    actor_id_t id;
    pthread_mutex_t *lock; 
    role_t *task;
    queue_t *q;
    void *state;
    bool godie;
    bool isReady;
    bool die;
} actor_info;

void execute2(actor_info *actor);

typedef struct systemAktorow {
    actor_id_t captain;
    size_t size;
    actor_id_t currentId;
    size_t amount;
    actor_info **array; // tablica aktorow
    tpool_t *pool;
    bool isGodie;
    pthread_t *singalExecutor;
    sigset_t *set;
    pthread_mutex_t *mutexSystem;
    pthread_cond_t *condSystem;
} aktory;

static aktory systemt;

aktory *getSystem() {
    return &systemt;
}


actor_info *actor_create(role_t *const role) {
    actor_info *actor = malloc(sizeof(actor_info));
    actor->lock = malloc(sizeof(pthread_mutex_t));
    if (pthread_mutex_init(actor->lock, NULL) != 0) {
        perror("actor mutex init failed\n");
        exit(1);
    }
    queue_new(&actor->q);
    actor->godie = false;
    actor->isReady = false;
    actor->die = false;
    actor->task = role;
    actor->state = NULL;
    actor->id = getSystem()->currentId;

    return actor;
}

void actor_destroy(actor_info *acto) {
    if (pthread_mutex_destroy(acto->lock) != 0) {
        perror("actor mutex destroy failed\n");
        return;
    }

    free(acto->state);
    free(acto->lock);
    queue_destroy(acto->q);
}

static void *sig_thread(void *arg) {
    sigset_t *set = arg;
    int s, sig;

    for (;;) {
        s = sigwait(set, &sig);
        if (s != 0) {
            perror("error sigwait\n");
            exit(1);
        }


        aktory *system_t = getSystem();
        if(system_t->isGodie == true){
            break;
        }
        pthread_mutex_lock(system_t->mutexSystem);

        system_t->isGodie = true;
        message_t gdMSG = {
                .message_type = MSG_GODIE
        };

        size_t j = 0;
        for (j = 0; j < system_t->currentId; ++j) {
            if(!system_t->array[j]->godie && !system_t->array[j]->die) {
                send_message(j, gdMSG);
            }
        }

        pthread_cond_broadcast(system_t->pool->condQ);
        pthread_mutex_unlock(system_t->mutexSystem);

        break;
    }

    
}

int actor_system_create(actor_id_t *actor, role_t *const role) {
    if (success > 0)
        return -1;
    else {
        //sprawdzanie czy mozna tworzyc tk czy poprzedni system sie zakonczyl
        //jakie locki na to?
        aktory *system_t = getSystem();
        // system = malloc(sizeof(aktory));

        system_t->singalExecutor = malloc(sizeof(pthread_t));
        system_t->set = malloc(sizeof(sigset_t));
        sigemptyset(system_t->set);
        sigaddset(system_t->set, SIGINT);

        if (pthread_sigmask(SIG_BLOCK, system_t->set, NULL) != 0) {
            perror("set sigmask failed\n");
            return -4;
        }

        if (pthread_create(system_t->singalExecutor, NULL, &sig_thread,
                           system_t->set) != 0) {
            perror("failed to create pthread\n");
            return -5;
        }

        success++;
        system_t->currentId = 0;
        system_t->captain = system_t->currentId;

        system_t->isGodie = false;
        system_t->size = 1024;
        system_t->amount = 1;
        system_t->array = malloc(system_t->size * sizeof(actor_info));
        system_t->pool = malloc(sizeof(tpool_t));
        thread_pool_create(system_t->pool, POOL_SIZE);
        if(system_t->isGodie == false )
            system_t->array[system_t->captain] = actor_create(role);
        // actor_create(&system->array[system->captain], role);
        system_t->currentId++;
        *actor = system_t->captain; // pierwszy ma ID = 0 ?

        system_t->mutexSystem = malloc(sizeof(pthread_mutex_t));
        system_t->condSystem = malloc(sizeof(pthread_cond_t));

        if (pthread_mutex_init(system_t->mutexSystem, NULL) != 0) {
            perror("system mutex init failed\n");
            return 1;
        }
        if (pthread_cond_init(system_t->condSystem, NULL) != 0) {
            perror("system cond init failed");
            return 1;
        }


        send_message(system_t->currentId - 1,
                     (message_t) {.message_type = MSG_HELLO, sizeof(void *),
                                                             NULL});
        return 0;
    }
}

bool empty(queue_t *q) {
    return (q->counter == 0);
}

bool full(queue_t *q) {
    return (q->counter == q->n);
}

void clear(queue_t *q) {
    q->beg = 0;
    q->i = -1;
    q->counter = 0;
}

void enqueue(queue_t *q, message_t *x) {
    if (q->counter == 0 && q->n == 0) {
        q->n = ACTOR_QUEUE_LIMIT;
        q->array = realloc(q->array, q->n * sizeof(void *));
    }

    if (q->counter >= q->n - 1) {
        perror("ERROR: KOLEJKA PELNA!!!!!!");
        return;
    }

    q->i = (q->i + 1) % q->n;
    q->array[q->i] = x;
    q->counter++;


}

void enqueue2(queue_t *q, void *x) {
    if (q->counter == 0 && q->n == 0) {
        q->n = ACTOR_QUEUE_LIMIT;
        q->array = realloc(q->array, q->n * sizeof(void *));
    }

    if (q->counter >= q->n - 1) {
        void **temp = malloc(2 * q->n * sizeof(void *));

        size_t v = q->beg;
        int j;

        for (j = 0; j < q->n; ++j) {
            temp[j] = q->array[v % q->n];
            v++;
        }

        q->n *= 2;
        free(q->array);
        q->array = temp;
        q->beg = 0;
        q->i = q->counter - 1;
    }

    q->i = (q->i + 1) % q->n;
    q->array[q->i] = x;
    q->counter++;
}

void *dequeue(queue_t *q) {
    void *temp = q->array[q->beg % q->n];
    q->beg = (q->beg + 1) % q->n;
    q->counter--;
    return temp;
}

void thread_pool_submit(tpool_t *pool, void *task) {// jw
    if (pthread_mutex_lock(pool->mutexQ) != 0) {
        perror("pool mutex lock failed\n");
        return;
    }

    enqueue2(pool->q, task);

    if (pthread_mutex_unlock(pool->mutexQ) != 0) {
        perror("pool mutex unlock failed\n");
        return;
    }

    if (pthread_cond_signal(pool->condQ)) {
        perror("pool cond signal failed\n");
        return;
    }
}

void total_clear(aktory *system_t) {
    for (int i = 0; i < system_t->currentId; ++i) {
        actor_destroy(system_t->array[i]);
        free(system_t->array[i]);
    }


    free(system_t->array);
    thread_pool_destroy(system_t->pool);
    free(system_t->pool);

    if(!system_t->isGodie){
        system_t->isGodie = true;
        if(pthread_kill(*system_t->singalExecutor, SIGINT) != 0){
            perror("Failed to send SIGINT to thread\n");
            exit(1);
        }

    }

    if (pthread_join(*system_t->singalExecutor, NULL)) {
        perror("Failed to join the thread");
        exit(1);
    }

    free(system_t->singalExecutor);
    sigemptyset(system_t->set);
    free(system_t->set);

    if (pthread_mutex_unlock(system_t->mutexSystem)) {
        perror("system mutex destroy failed\n");
        return;
    }


    if (pthread_cond_destroy(system_t->condSystem)) {
        perror("system cond destroy failed\n");
        return;
    }

    system_t->amount = 0;
    system_t->size = 0;
    system_t->currentId = 0;
    system_t->captain = 0;

    free(system_t->mutexSystem);
    free(system_t->condSystem);
}

void actor_system_join(actor_id_t actor) {
    //sprawdzanie czy wgl taki aktor jest w systmie? bo i tak chyba system jest tylko jeden

    if (success == 0)
        return;


    aktory *system_t = getSystem();

    if (actor > system_t->currentId - 1 || system_t->array[actor] == NULL)
        return;


    if (pthread_mutex_lock(system_t->mutexSystem) != 0) {
        perror("system mutex lock failed\n");
        return;
    }

    while (system_t->amount > 0) {
        if (pthread_cond_wait(system_t->condSystem, system_t->mutexSystem) !=
            0) {
            perror("system cond wait failed\n");
            return;
        }
    }


    success = 0;

    if (pthread_cond_broadcast(system_t->pool->condQ) != 0) {
        perror("system pool cond signal failed\n");
        return;
    }


    total_clear(system_t);

}




//Zwykle wysłanie komunikatu MSG_GODIE musi być poprzedzone wysłaniem komunikatu przeprowadzającego zwalnianie zasobów,
//którymi gospodaruje aktor (zaalokowana pamięć, zajęte deskryptory plików itp.)

void godie_fun(void **stateptr, size_t nbytes, void *data) {
    actor_id_t myId = actor_id_self();
    aktory *system = getSystem();

    if (pthread_mutex_lock(system->mutexSystem)) {
        perror("system mutex lock failed\n");
        return;
    }

    actor_info *tempActor = system->array[myId];

    if (pthread_mutex_unlock(system->mutexSystem)) {
        perror("system mutex unlock failed");
        return;
    }
    tempActor->godie = true;

}

void spawn_fun(void **stateptr, size_t nbytes, void *data) {
    actor_id_t myId = actor_id_self();

    aktory *system_t = getSystem();

    if(system_t->isGodie){
        perror("NIIE MOZNA JUZ TWORZYC NOWYCH AKTOROW\n");
        return;
    }
    if (pthread_mutex_lock(system_t->mutexSystem)) {
        perror("system mutex lock failed\n");
        return;
    }

    if (system_t->currentId >= CAST_LIMIT - 1) {
        if (pthread_mutex_unlock(system_t->mutexSystem)) {
            perror("system mutex unlock failed\n");
            return;
        }
        perror("Failed to create actor. To many actors\n");
        return;
    }

    if (system_t->currentId >= system_t->size - 1) {
        system_t->array = realloc(system_t->array,
                                  2 * system_t->size * sizeof(actor_info));
        system_t->size *= 2;
    }


    system_t->array[system_t->currentId] = actor_create(data);
    system_t->currentId++;
    system_t->amount++;

    if (pthread_mutex_unlock(system_t->mutexSystem)) {
        perror("system mutex unlock failed\n");
        return;
    }

    send_message(system_t->currentId - 1,
                 (message_t) {.message_type = MSG_HELLO, sizeof(actor_id_t),
                                                         (void *) myId});
}


int send_message(actor_id_t actor, message_t msg) {

    //return 0 jak ok
    //return -1 jak aktor ma godie?
    //return -2 jak aktora nie ma

    aktory *system_t = getSystem();
    if (actor >= system_t->currentId)
        return -2;
    else {

        actor_info *tempActor = system_t->array[actor];
        if (tempActor == NULL)
            return -2;


        if (tempActor->godie)
            return -1;


        //do kolejaki aktora wkladnie wiadomosci
        pthread_mutex_lock(tempActor->lock);

        message_t *mem = malloc(sizeof(message_t));
        mem->message_type = msg.message_type;
        mem->nbytes = msg.nbytes;
        mem->data = msg.data;

        if (empty(tempActor->q) && !tempActor->isReady) {
            tempActor->isReady = true;
            enqueue(tempActor->q, mem);

            thread_pool_submit(system_t->pool, (void *) tempActor->id);

        } else {

            enqueue(tempActor->q, mem);
        }


        pthread_mutex_unlock(tempActor->lock);
        //ogarniac returny
        return 0;
    }
}

static void *execute(void *pool) {
    aktory *system_t = getSystem();
    tpool_t *temp = (tpool_t *) pool;

    while (true) {

        actor_id_t actorId;
        if (system_t->amount == 0 && success == 0)
            return 0;

        pthread_mutex_lock(temp->mutexQ);
        while (empty(temp->q)) {
            if (system_t->amount == 0 && success == 0)
                break;

            pthread_cond_wait(temp->condQ, temp->mutexQ);

        }


        if (system_t->amount == 0 && success == 0) {
            pthread_cond_broadcast(temp->condQ);
            pthread_mutex_unlock(temp->mutexQ);
            break;
        }


        actorId = (actor_id_t) dequeue(temp->q);
        actor_info *tempActor = system_t->array[actorId];
        pthread_mutex_lock(tempActor->lock);
        pthread_mutex_unlock(temp->mutexQ);
        //wykonaj operacje na acotrId
        execute2(tempActor);
    }

    return 0;
}

void execute2(actor_info *tempActor) {
    aktory *system_t = getSystem();

    idGlob = tempActor->id;

    message_t *message = (message_t *) dequeue(tempActor->q);

    pthread_mutex_unlock(tempActor->lock);


    if (message->message_type == MSG_SPAWN) {
        spawn_fun(&tempActor->state, message->nbytes, message->data);
    } else if (message->message_type == MSG_GODIE) {

        godie_fun(&tempActor->state, message->nbytes, message->data);
    } else {

        if (message->message_type < tempActor->task->nprompts)
            tempActor->task->prompts[message->message_type](&tempActor->state,
                                                            message->nbytes,
                                                            message->data);
    }

    free(message);
    pthread_mutex_lock(tempActor->lock);

    if (!tempActor->die && tempActor->godie && empty(tempActor->q)) {
        pthread_mutex_lock(system_t->mutexSystem);
        system_t->amount--;
        tempActor->die = true;

        pthread_mutex_unlock(tempActor->lock);

        if (system_t->amount == 0) {
            pthread_mutex_unlock(system_t->mutexSystem);
            pthread_cond_signal(system_t->condSystem);

        } else {
            pthread_mutex_unlock(system_t->mutexSystem);
        }
    } else if (!empty(tempActor->q)) {

        thread_pool_submit(system_t->pool, (void *) tempActor->id);
        pthread_mutex_unlock(tempActor->lock);
    } else {
        if (empty(tempActor->q))
            tempActor->isReady = false;


        pthread_mutex_unlock(tempActor->lock);
    }
}
