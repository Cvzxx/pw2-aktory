

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include "cacti.h"


unsigned long long n;
int ct;
static role_t role;

role_t *getRole() {
    return &role;
}

typedef struct factorial {
    unsigned long long prefVal;
    unsigned int k;
    actor_id_t acId;

} fac_t;


void myState(void **stateptr, size_t size, void *data) {

    fac_t temptState;
    if(((fac_t *) * stateptr)->prefVal == 0) {
        temptState.prefVal = 1;
        temptState.k = 1;
    }
    else {
        temptState.prefVal = ((fac_t *) *stateptr)->prefVal;
        temptState.k = ((fac_t *) *stateptr)->k + 1;
    }


    temptState.prefVal *= temptState.k;


    ((fac_t *) data)->k = temptState.k;
    ((fac_t *) data)->prefVal = temptState.prefVal;
    message_t secMsg = {
            .data = &n,
            .message_type = 2
    };


    send_message(((fac_t *) data)->acId, secMsg);

    message_t gdMSG = {
            .message_type = MSG_GODIE
    };
    send_message(actor_id_self(), gdMSG);
}

void count(void **stateptr, size_t size, void *data) {

    if (((fac_t *) *stateptr)->k == *((unsigned long long *) data)) {
        printf("%llu\n", ((fac_t *) *stateptr)->prefVal);
        message_t gdMSG = {
                .message_type = MSG_GODIE
        };
        send_message(actor_id_self(), gdMSG);
    } else {
        message_t msgSp = {
                .message_type = MSG_SPAWN,
                .data = getRole()
        };

        send_message(actor_id_self(), msgSp);
    }
}

void hello(void **stateptr, size_t size, void *data) {
    // assert(*stateptr == NULL);
    //   sleep(10);
    if ( ct == 0) {
        ct++;
        fac_t *tempState = malloc(sizeof(fac_t));
        tempState->prefVal = 1;
        tempState->k = 0;
        tempState->acId = actor_id_self();

        *stateptr = tempState;
        message_t secMsg = {
                .data = &n,
                .message_type = 2
        };

        send_message(actor_id_self(), secMsg);

    } else {
        fac_t *tempState = malloc(sizeof(fac_t));
        tempState->prefVal = 0;
        tempState->k = 0;
        tempState->acId = actor_id_self();
        *stateptr = tempState;


        message_t stateMSG = {
                .data = *stateptr,
                .message_type = 1
        };


        send_message(((long long  ) data), stateMSG);

    }


}

int main() {



    scanf("%llu", &n);


    role_t *tempRole = getRole();
    tempRole->nprompts = 3;
    void (**prompts)(void **, size_t, void *) = malloc(
            tempRole->nprompts * sizeof(void *));
    prompts[0] = &hello;
    prompts[1] = &myState;
    prompts[2] = &count;

    tempRole->prompts = prompts;


    actor_id_t actorId;
    actor_system_create(&actorId, getRole());
    //  sleep(10);
    actor_system_join(0);
//    actor_system_join(0);

    free(prompts);
    // free(getRole());
}
