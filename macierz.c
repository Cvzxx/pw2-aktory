#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "cacti.h"

long long **matrixValues;
long long **matrixTimes;
long long k;
long long n;

int ct;
static role_t role;

role_t *getRole() {
    return &role;
}

typedef struct column {
    long long *array;
    long long *time;
    size_t len;
    int counter;
    int childID;
    actor_id_t acId;
    actor_id_t parId;

} col;

typedef struct val {
    long long v;
    long long row;
} value_t;

void myState(void **stateptr, size_t size, void *data) {
    long long i;
    long long colId = ((col *) *stateptr)->childID;

    for (i = 0; i < n; ++i) {
        ((col *) data)->array[i] = matrixValues[i][colId];
        ((col *) data)->time[i] = matrixTimes[i][colId];
    }

    ((col *) *stateptr)->childID++;


    message_t secMsg = {
            .data = &n,
            .message_type = 2
    };

    send_message(((col *) data)->acId, secMsg);


}

void sum(void **stateptr, size_t size, void *data) {

    long long rowId = ((value_t *) data)->row;
    long long vTemp = ((value_t *) data)->v;

    ((col *) *stateptr)->array[rowId] += vTemp;
    ((col *) *stateptr)->counter++;

    free(data);
    if (((col *) *stateptr)->counter == n * k) {
        long long i;

        for (i = 0; i < n; ++i) {
            long long valuePrint = ((col *) *stateptr)->array[i];
            printf("%llu\n", valuePrint);
        }


        free(((col *) *stateptr)->array);
        message_t gdMSG = {
                .message_type = MSG_GODIE
        };
        send_message(actor_id_self(), gdMSG);
    }


}

void count(void **stateptr, size_t size, void *data) {

    long long times = ((col *) *stateptr)->len;

    long long i;

    for (i = 0; i < times; ++i) {
        usleep(((col *) *stateptr)->time[i] * 10000);

        value_t *tempValue = malloc(sizeof(value_t));
        tempValue->v = ((col *) *stateptr)->array[i];
        tempValue->row = i;

        message_t sumMSG = {
                .message_type = 3,
                .data = tempValue
        };

        send_message(((col *) *stateptr)->parId, sumMSG);
    }

    free(((col *) *stateptr)->time);
    free(((col *) *stateptr)->array);


    message_t gdMSG = {
            .message_type = MSG_GODIE
    };
    send_message(actor_id_self(), gdMSG);
}

void hello(void **stateptr, size_t size, void *data) {

    if (ct == 0) {
        ct++;
        col *tempState = malloc(sizeof(col));
        tempState->array = malloc(sizeof(long long) * n);
        long long ii;
        for(ii = 0; ii < n; ++ii)
            tempState->array[ii] = 0;
        tempState->time = NULL;
        tempState->len = n;
        tempState->childID = 0;
        tempState->counter = 0;
        tempState->acId = actor_id_self();

        *stateptr = tempState;


    } else {
        col *tempState = malloc(sizeof(col));
        tempState->array = malloc(sizeof(long long) * n);
        tempState->time = malloc(sizeof(long long) * n);
        tempState->len = n;
        tempState->childID = 0;
        tempState->counter = 0;
        tempState->acId = actor_id_self();
        tempState->parId = (long long )  data;

        *stateptr = tempState;


        message_t staeMsg = {
                .data = *stateptr,
                .message_type = 1
        };

        send_message( (long long ) data, staeMsg);

    }


}

int main() {

    scanf("%lld", &n);
    scanf("%lld", &k);


    matrixTimes = malloc(sizeof(long long *) * n);
    matrixValues = malloc(sizeof(long long *) * n);

    long long ii = 0;
    for (; ii < n; ++ii) {
        matrixValues[ii] = malloc(sizeof(long long) * k);
        matrixTimes[ii] = malloc(sizeof(long long) * k);
    }


    for (int i = 0; i < n; i++) {
        for (int j = 0; j < k; j++) {
            scanf("%lld", &matrixValues[i][j]);
            scanf("%lld", &matrixTimes[i][j]);
        }
    }


    role_t *tempRole = getRole();
    tempRole->nprompts = 4;
    void (**prompts)(void **, size_t, void *) = malloc(
            tempRole->nprompts * sizeof(void *));
    prompts[0] = &hello;
    prompts[1] = &myState;
    prompts[2] = &count;
    prompts[3] = &sum;

    tempRole->prompts = prompts;

    message_t msgSp = {
            .message_type = MSG_SPAWN,
            .data = getRole()
    };


    actor_id_t actorId;
    actor_system_create(&actorId, getRole());
    int i;
    for (i = 0; i < k; ++i) {
        send_message(actorId, msgSp);
    }

    actor_system_join(0);


    ii = 0;
    for (; ii < n; ++ii) {
        free(matrixValues[ii]);
        free(matrixTimes[ii]);
    }

    free(matrixTimes);
    free(matrixValues);
    free(prompts);

    // free(getRole());
}
